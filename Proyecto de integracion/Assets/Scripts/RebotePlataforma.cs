using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RebotePlataforma : MonoBehaviour
{
    public float bounceForce = 10f;
    private Collider2D plataforma;

    private void Start()
    {
        plataforma = GetComponent<Collider2D>();
    }

    private void FixedUpdate()
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(plataforma.bounds.center, plataforma.bounds.size, 0f);

        // Recorre cada objeto encontrado
        foreach (Collider2D collider in colliders)
        {
            // Verifica si el objeto tiene un Rigidbody2D (es decir, si puede rebotar)
            Rigidbody2D rb = collider.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                // Aplica una fuerza hacia arriba para hacer rebotar el objeto
                rb.velocity = new Vector2(rb.velocity.x, bounceForce);
            }
        }
    }
}