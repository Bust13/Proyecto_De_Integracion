using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PjControl : MonoBehaviour
{
    public float velocidad;
    public float fuerzaDeSalto;
    public float saltoMaximo;
    public LayerMask Piso;

    private Rigidbody2D rb;
    private BoxCollider2D boxCollider;
    private Animator animator;
    private PlayerInput1 inputActions;

    private bool mirandoDerecha = true;
    private float saltosRestantes;
    private Vector3 escalaInicial;
    private float direccionMovimiento = 1f; // Variable para guardar la dirección de movimiento
    [SerializeField]
    GameObject notaText;

    [SerializeField]
    GameObject panelCodigo, puertaCerrada, number;

    public static bool estaAbiera = false;
    public static bool notaAbierta= false;

    private void Awake()
    {
        inputActions = new PlayerInput1();
        inputActions.Player.Move.performed += ctx => Movimiento(ctx.ReadValue<Vector2>());
        inputActions.Player.Move.canceled += ctx => Movimiento(Vector2.zero);
        inputActions.Player.Jump.performed += ctx => Salto();
        inputActions.Player.SizeDecrease.started += ctx => CambiarTamanio(0.5f); // Reducir tamaño
        inputActions.Player.SizeIncrease.started += ctx => VolverAlTamañoOriginal();
    }

    private void OnEnable()
    {
        inputActions.Player.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player.Disable();
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        saltosRestantes = saltoMaximo;
        animator = GetComponent<Animator>();
        escalaInicial = transform.localScale;
        panelCodigo.SetActive(false);
        puertaCerrada.SetActive(true);
        notaText.SetActive(false);
        number.SetActive(false) ;

        
    }
    private void Update()
    {
        if (estaAbiera)
        {
            panelCodigo.SetActive(false);
            number.SetActive(false);
            puertaCerrada.SetActive(false);
        }
        if (notaAbierta)
        {
            notaText.SetActive(true);
        }
        else notaText.SetActive(false);
    }

    private void Movimiento(Vector2 inputMovimiento)
    {
        float movimientoHorizontal = inputMovimiento.x * direccionMovimiento * velocidad;

        rb.velocity = new Vector2(movimientoHorizontal, rb.velocity.y);

        animator.SetBool("EstaCaminando", Mathf.Abs(inputMovimiento.x) > 0.1f);

        if (inputMovimiento.x > 0 && !mirandoDerecha)
        {

            Girar();
        }
        else if (inputMovimiento.x < 0 && mirandoDerecha)
        {

            Girar();
        }
    }

    private void Girar()
    {
        mirandoDerecha = !mirandoDerecha;
        transform.Rotate(0f, 180f, 0f);
    }

    private bool EstaEnSuelo()
        {
            RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, 0.2f, Piso);
            return raycastHit.collider != null;
        }

        private void Salto()
        {
            if (EstaEnSuelo())
            {
                saltosRestantes = saltoMaximo;
            }

            if (saltosRestantes > 0)
            {
                saltosRestantes--;
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                rb.AddForce(Vector2.up * fuerzaDeSalto, ForceMode2D.Impulse);
                animator.SetTrigger("Salta");
            }
        }

        private void CambiarTamanio(float factor)
        {
            transform.localScale = new Vector3(escalaInicial.x * factor, escalaInicial.y * factor, escalaInicial.z);
        }

        private void VolverAlTamañoOriginal()
        {

            transform.localScale = escalaInicial;
        }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("ParedCerrada2") && !estaAbiera && !notaAbierta)
        {
            panelCodigo.SetActive(true);
            number.SetActive(true);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("Nota"))
        {
            notaAbierta = true;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("ParedCerrada2"))
        {
            panelCodigo.SetActive(false);
            number.SetActive(false);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
  

        if (collision.CompareTag("Nota"))
        {
            notaAbierta = false;
        }
    }
}
