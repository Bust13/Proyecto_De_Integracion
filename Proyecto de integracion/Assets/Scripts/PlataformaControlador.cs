using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaControlador : MonoBehaviour
{
    [SerializeField] private float tiempoEspera;
    [SerializeField] private float tiempoReset;
    [SerializeField] private float velocidadRotacion;
    [SerializeField] private Color colorOscuro;
    [SerializeField] private Color colorCaida;
    [SerializeField] private float duracionCaidaColor = 1f;  // Tiempo durante el cual el color sigue cambiando despu�s de la ca�da

    private Rigidbody2D rigidbody2;
    private SpriteRenderer spriteRenderer;
    private Color colorInicial;
    private Vector2 posicionInicial;
    private Quaternion rotacionInicial;
    private bool caida = false;

    private void Start()
    {
        rigidbody2 = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        posicionInicial = transform.position;
        rotacionInicial = transform.rotation;
        colorInicial = spriteRenderer.color;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Caida(collision));
        }
    }

    private IEnumerator Caida(Collision2D other)
    {
        float tiempoTranscurrido = 0;
        while (tiempoTranscurrido < tiempoEspera)
        {
            spriteRenderer.color = Color.Lerp(colorInicial, colorOscuro, tiempoTranscurrido / tiempoEspera);
            tiempoTranscurrido += Time.deltaTime;
            yield return null;
        }

        spriteRenderer.color = colorOscuro;

        caida = true;
        Physics2D.IgnoreCollision(transform.GetComponent<Collider2D>(), other.transform.GetComponent<Collider2D>());
        rigidbody2.constraints = RigidbodyConstraints2D.None;
        rigidbody2.AddForce(new Vector2(0.1f, 0));

        tiempoTranscurrido = 0;
        while (tiempoTranscurrido < duracionCaidaColor)
        {
            spriteRenderer.color = Color.Lerp(colorOscuro, colorCaida, tiempoTranscurrido / duracionCaidaColor);
            tiempoTranscurrido += Time.deltaTime;
            yield return null;
        }

        spriteRenderer.color = colorCaida;

        yield return new WaitForSeconds(tiempoReset);
        ResetPlataforma();
    }

    private void ResetPlataforma()
    {
        caida = false;
        rigidbody2.constraints = RigidbodyConstraints2D.FreezeAll;
        transform.position = posicionInicial;
        transform.rotation = rotacionInicial;
        spriteRenderer.color = colorInicial;
        rigidbody2.velocity = Vector2.zero;
        rigidbody2.angularVelocity = 0f;
    }
}
