using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PanelContraseña2 : MonoBehaviour
{
    [SerializeField]

    TMP_Text codigoText2;
    string codigoValor2 = "";

    private void Update()
    {
        codigoText2.text = codigoValor2;

        if (codigoValor2 == "8923")
        {
            PjControl.estaAbiera = true;
        }
        if (codigoValor2.Length >= 4)
        {
            codigoValor2 = "";
        }
    }
    public void AddDigit(string digito)
    {
        codigoValor2 += digito;
    }
}
