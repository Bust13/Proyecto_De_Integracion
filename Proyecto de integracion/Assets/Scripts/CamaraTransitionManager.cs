using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraTransitionManager : MonoBehaviour
{
    public List<Transform> cameraPositions; 
    public float cameraSpeed = 5f; 
    private int currentPosIndex = 0;
    private void Start()
    {
        if (cameraPositions.Count > 0)
        {
            Camera.main.transform.position = cameraPositions[0].position; 
        }
    }

    public void MoverProxPosicion()
    {
        if (currentPosIndex < cameraPositions.Count - 1)
        {
            currentPosIndex++;
            StartCoroutine(MoverCamera(cameraPositions[currentPosIndex].position));
        }
    }
    private IEnumerator MoverCamera(Vector3 targetPosition)
    {
        while (Vector3.Distance(Camera.main.transform.position, targetPosition) > 0.1f)
        {
            Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, targetPosition, cameraSpeed * Time.deltaTime);
            yield return null;
        }
    }
    void Update()
    {
        
    }
}
