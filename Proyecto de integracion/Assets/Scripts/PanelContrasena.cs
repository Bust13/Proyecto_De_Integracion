using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PanelContrasena : MonoBehaviour
{
    [SerializeField]
     
    TMP_Text codigoText;
    string codigoValor = "";

    private void Update()
    {
        codigoText.text = codigoValor;

        if(codigoValor == "1234")
        {
            Pj2Control.estaAbiera = true;
        }
        if (codigoValor.Length >= 4)
        {
            codigoValor = "";
        }
    }
    public void AddDigit(string digito)
    {
        codigoValor += digito;
    }
}
